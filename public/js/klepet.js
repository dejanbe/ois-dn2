var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};

Klepet.prototype.spremeniKanalZGeslom = function(kanal, geslo) {
	this.socket.emit('pridruzitevZahtevaZGeslom', {
		novKanal: kanal,
		geslo: geslo
	});
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;
  var re = /\"(.*?)\"\s\"(.*?)\"/;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
	  if(re.test(kanal)){
		this.spremeniKanalZGeslom(RegExp.$1, RegExp.$2);
	  }else{
	  	this.spremeniKanal(kanal);
	  }
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
	  besede.shift();
	  var zasebno = besede.join(' ');
	  if(re.test(zasebno)){
	  	this.socket.emit('zasebnoSporociloZahteva', zasebno);
	  }else{
		  sporocilo = 'Neznan ukaz.';
	  }
	  break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};
