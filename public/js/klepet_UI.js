function censor(tekst){
	var profanity = "**********************"
	var profanities = $('#swearWords').text().split("\n");
	for(i=0; i<profanities.length; i++){
		var regex = new RegExp("(^|\\W+)" + profanities[i] + "($|\\W+)","gi");
		tekst = tekst.replace(regex, "$1" + profanity.substr(0, profanities[i].length) + "$2");
	}
	return tekst;
}

function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}
 
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function divElementTekstInSmajli(sporocilo) {
	var smajliji = [':)', ';)', '(y)', ":*", ':('];
	var povezave = ['<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png"/>','<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png"/>','<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png"/>','<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png"/>','<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png"/>'];
	/* manual .text(), ker iz DOM objekta ne znam izlusciti stringa */
	var stuff = String(sporocilo).replace(/&/g, "&amp;").replace(/</g, "&lt;")
		.replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#039;")
		.replace(/\//g, "&#x2F;");
	for(i=0; i<smajliji.length; i++){
		stuff = stuff.replace(smajliji[i], povezave[i]);
	}
	return $('<div style="font-weight: bold"></div>').html(stuff);
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    $('#sporocila').append(divElementTekstInSmajli(censor(sporocilo)));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
    $('#naziv').text(rezultat.vzdevek);
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    $('#swearWords').load("swearWords.txt");
    $('#kanal').text(rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('pridruzitevNegativniOdgovor', function(sporocilo) {
	$('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = divElementTekstInSmajli(censor(sporocilo.besedilo));
    $('#sporocila').append(novElement);
  });

  socket.on('vzdevki', function(vzdevki) {
    $('#seznam-vzdevkov').empty();
	for(i=0; i<vzdevki.length; i++){
		$('#seznam-vzdevkov').append(divElementEnostavniTekst(vzdevki[i]));
	}
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  socket.on('zasebnoSporociloOdgovor', function(sporocilo) {
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('zasebnoSporocilo', function(sporocilo) {
    $('#sporocila').append(divElementTekstInSmajli(censor(sporocilo)));
  });

  setInterval(function() {
    socket.emit('kanali');
	socket.emit('vzdevki2');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
