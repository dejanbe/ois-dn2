var http = require('http');
var fs  = require('fs');
var path = require('path');
var mime = require('mime');
var predpomnilnik = {};

function posredujNapako404(odgovor) {
  odgovor.writeHead(404, {'Content-Type': 'text/plain'});
  odgovor.write('Napaka 404: Vira ni mogoče najti.');
  odgovor.end();
}

function posredujDatoteko(odgovor, datotekaPot, datotekaVsebina) {
  odgovor.writeHead(200, {"content-type": mime.lookup(path.basename(datotekaPot))});
  odgovor.end(datotekaVsebina);
}

function posredujStaticnoVsebino(odgovor, predpomnilnik, absolutnaPotDoDatoteke) {
  if (predpomnilnik[absolutnaPotDoDatoteke]) {
    posredujDatoteko(odgovor, absolutnaPotDoDatoteke, predpomnilnik[absolutnaPotDoDatoteke]);
  } else {
    fs.exists(absolutnaPotDoDatoteke, function(datotekaObstaja) {
      if (datotekaObstaja) {
        fs.readFile(absolutnaPotDoDatoteke, function(napaka, datotekaVsebina) {
          if (napaka) {
            posredujNapako404(odgovor);
          } else {
            predpomnilnik[absolutnaPotDoDatoteke] = datotekaVsebina;
            posredujDatoteko(odgovor, absolutnaPotDoDatoteke, datotekaVsebina);
          }
        });
      } else {
        posredujNapako404(odgovor);
      }
    });
  }
}

var streznik = http.createServer(function(zahteva, odgovor) {
  var potDoDatoteke = false;

  if (zahteva.url == '/') {
    potDoDatoteke = 'public/index.html';
  } else {
    potDoDatoteke = 'public' + zahteva.url;
  }

  var absolutnaPotDoDatoteke = './' + potDoDatoteke;
  posredujStaticnoVsebino(odgovor, predpomnilnik, absolutnaPotDoDatoteke);
});

streznik.listen(8080, function() {
  console.log("Strežnik posluša na portu " + 8080 + ".");
});

var klepetalnicaStreznik = require('./lib/klepetalnica_streznik');
klepetalnicaStreznik.listen(streznik);
