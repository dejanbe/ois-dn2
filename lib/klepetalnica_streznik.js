var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var socketiGledeNaVzdevek = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var gesla = {};

exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
	posredujZasebnoSporocilo(socket, uporabljeniVzdevki);
    obdelajPridruzitevKanaluZGeslom(socket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
	socket.on('vzdevki2', function() {
		oddajSeznamVzdevkov(socket);
	});
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socketiGledeNaVzdevek[vzdevek] = socket.id;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

function oddajSeznamVzdevkov(socket) {
	var kanal = trenutniKanal[socket.id];
	var vzdevki = io.sockets.clients(kanal);
	var seznam = [];
	for( var i in vzdevki){
		seznam.push(vzdevkiGledeNaSocket[vzdevki[i].id]);
	}
	socket.emit('vzdevki', seznam);
	socket.broadcast.to(kanal).emit('vzdevki', seznam);
}

function pridruzitevKanalu(socket, kanal) {
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {kanal: kanal});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });

  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
		socketiGledeNaVzdevek[vzdevek] = socket.id;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
		delete socketiGledeNaVzdevek[prejsnjiVzdevek];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
	if(gesla[kanal.novKanal] != undefined){
		socket.emit('pridruzitevNegativniOdgovor', "Kanal " + kanal.novKanal + " je zasciten z geslom. Prijavite se z ukazom /pridruzitev \"kanal\" \"geslo\"");
	}else{
		socket.leave(trenutniKanal[socket.id]);
		pridruzitevKanalu(socket, kanal.novKanal);
	}
  });
}

function posredujZasebnoSporocilo(socket, uporabljeniVzdevki) {
	socket.on('zasebnoSporociloZahteva', function(niz) {
		var razdeljen = niz.split("\" \"");
		var vzdevek = razdeljen[0].slice(razdeljen[0].indexOf("\"")+1);
		var tekst = razdeljen[1].slice(0, razdeljen[1].indexOf("\""));
		if(uporabljeniVzdevki.indexOf(vzdevek) == -1 || vzdevek == vzdevkiGledeNaSocket[socket.id]){
			socket.emit('zasebnoSporociloOdgovor', "Sporocila \"" + tekst + "\" uporabniku z vzdevkom \"" + vzdevek + "\" ni bilo mogoce posredovati.");
		}else{
			io.sockets.socket(socketiGledeNaVzdevek[vzdevek]).emit('zasebnoSporocilo', vzdevkiGledeNaSocket[socket.id] + " (zasebno): " + tekst);
			socket.emit('zasebnoSporocilo', "(zasebno za " + vzdevek + "): " + tekst );
		}
	});
}

function obdelajPridruzitevKanaluZGeslom(socket) {
	socket.on('pridruzitevZahtevaZGeslom', function(kanal){
		if(gesla[kanal.novKanal] == undefined){
			var exists = false;
			for(var trenutniKanali in io.sockets.manager.rooms){
				trenutniKanali = trenutniKanali.substring(1, trenutniKanali.length);
				if(trenutniKanali == kanal.novKanal){
					exists = true;
					break;
				}
			}
			if(kanal.novKanal == "Skedenj") exists = true;
			if(exists){
				socket.emit('pridruzitevNegativniOdgovor', "Izbrani kanal " + kanal.novKanal + " je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev <kanal> ali zahtevajte kreiranje kanala z drugim imenom.");
			}else{
				gesla[kanal.novKanal] = kanal.geslo;
				socket.leave(trenutniKanal[socket.id]);
				pridruzitevKanalu(socket, kanal.novKanal);
			}
		}else if(gesla[kanal.novKanal] == kanal.geslo){
			socket.leave(trenutniKanal[socket.id]);
			pridruzitevKanalu(socket, kanal.novKanal);
		}else{
			//napacno geslo
			socket.emit('pridruzitevNegativniOdgovor', "Pridruzitev v kanal " + kanal.novKanal + " ni bila uspesna, ker je geslo napacno.");
		}
	});
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
	delete socketiGledeNaVzdevek[vzdevkiGledeNaSocket[socket.id]];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}
